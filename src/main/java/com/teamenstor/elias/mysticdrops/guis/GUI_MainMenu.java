package com.teamenstor.elias.mysticdrops.guis;

import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import com.teamenstor.elias.mysticdrops.utility.ItemUtility;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public class GUI_MainMenu extends InventoryGUI {

	public GUI_MainMenu () {
		super(27, ChatColor.DARK_AQUA + "MysticDrops - Main Menu", GUIType.UNEDITABLE);
		try {
			setItem(12, ItemUtility.createItem(Material.GRASS, ChatColor.AQUA + "Blocks"), (player, clickEvent) -> {
				GUIManager.getBlocksMain().open(player);
			});
			setItem(14, ItemUtility.createSpawnEgg(EntityType.ZOMBIE, ChatColor.AQUA + "Mobs"), (player, clickEvent) -> {
				player.sendMessage(ChatColor.GREEN + "Clicked spawn egg!");
			});
			ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), " ");
			for (int i = 0; i < getSize(); i++) {
				if (getItem(i) == null) { setItem(i, extra); }
			}
		}catch(Exception exception){ MessageUtility.logException(exception, getClass()); }
	}
}
