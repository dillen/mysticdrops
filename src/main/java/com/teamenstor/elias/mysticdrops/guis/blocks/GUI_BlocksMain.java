package com.teamenstor.elias.mysticdrops.guis.blocks;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.ItemUtility;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.SaveUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class GUI_BlocksMain extends InventoryGUI {

	private int page = 0;

	public GUI_BlocksMain () {
		super(54, ChatColor.DARK_AQUA + "MysticDrops - Blocks", GUIType.UNEDITABLE);
	}

	@Override
	public void updateItems () {
		try {
			getInventory().clear();

			/* Adding contents of list */
			int startPos = 21 * page;

			if (startPos >= Main.blockDrops.size())
				startPos = Main.blockDrops.size() - 21;
			if (startPos < 0)
				startPos = 0;

			int endPos = startPos + 21;

			int pos = 10;

			Set<ItemStack> items = Main.blockDrops.keySet().stream().filter(Objects::nonNull).collect(Collectors.toSet());
			for (int i = startPos; i < endPos; i++) {
				for (int x = 0; x < 7; x++) {
					if (i >= items.size()) {
						ItemStack white_glass = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 8), " ");
						setItem(pos, white_glass);
					} else {
						ItemStack rawItem = ((ItemStack) items.toArray()[i]).clone();
						ItemStack item = rawItem.clone();
						item = ItemUtility.createItem(item, ChatColor.AQUA + MessageUtility.getItemName(item), Arrays.asList(ChatColor.GRAY + "Click to edit.", ChatColor.GRAY + "Shift-click to remove."));
						setItem(pos, item, (player, clickEvent) -> {
							if (clickEvent.isShiftClick()) {
								GUIManager.getConfirm().deleteQuery(rawItem.clone()).open(player);
							} else {
								player.sendMessage(ChatColor.GREEN + "Clicked \"" + rawItem.getType().name() + "\".");
							}
						});
					}
					i++;
					pos++;
				}
				i--;
				pos += 2;
			}

			setItem(49, ItemUtility.createItem(Material.EMERALD_BLOCK, ChatColor.GREEN + "Add block"), ((player, clickEvent) -> {
				GUIManager.getAddBlock().open(player);
			}));

			if (page == 0) {
				setItem(46, ItemUtility.createItem(Material.ARROW, ChatColor.RED + "Back"), ((player, clickEvent) -> {
					GUIManager.getMainMenu().open(player);
				}));
			} else {
				setItem(46, ItemUtility.createItem(Material.ARROW, ChatColor.RED + "Previous page"), ((player, clickEvent) -> {
					page--;
					updateItems();
				}));
			}
			if (Main.blockDrops.size() > 21 * (page + 1)) {
				setItem(52, ItemUtility.createItem(Material.ARROW, ChatColor.RED + "Next page"), ((player, clickEvent) -> {
					page++;
					updateItems();
				}));
			}

			ItemStack extra = ItemUtility.createItem(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7), " ");
			for (int i = 0; i < getSize(); i++) {
				if (getItem(i) == null) {
					setItem(i, extra);
				}
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

	@Override
	public void open (Player player) {
		updateItems();
		super.open(player);
	}

	@Override
	public void closeExecutions (Player player) {
		if ((Boolean) Main.configManager.getConfig("config.yml").get("autosave")) {
			SaveUtility.saveBlockDrops();
		}
		delete();
	}
}
