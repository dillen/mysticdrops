package com.teamenstor.elias.mysticdrops.events;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.InventoryGUI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

public class GUIListener implements Listener {

	/* Basic click and drag events, executes action if found */

	private void inventoryInteractEvent (InventoryInteractEvent rawEvent) {
		try {
			InventoryClickEvent event = (InventoryClickEvent) rawEvent;
			if (!(event.getWhoClicked() instanceof Player)) return;

			Player player = (Player) event.getWhoClicked();
			UUID playerUUID = player.getUniqueId();
			UUID inventoryUUID = InventoryGUI.getOpenInventories().get(playerUUID);

			if (inventoryUUID != null) {
				InventoryGUI gui = InventoryGUI.getInventoriesByUUID().get(inventoryUUID);
				if (gui.getType() == InventoryGUI.GUIType.UNEDITABLE) { event.setCancelled(true); }
				InventoryGUI.GUIAction action = gui.getActions().get(event.getSlot());
				if (action != null && event.getClickedInventory().getTitle().equals(gui.getInventory().getName())) { action.click(player, event); }
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

	@EventHandler
	public void onClick (InventoryClickEvent event) { inventoryInteractEvent(event); }

	@EventHandler
	public void onDragEvent (InventoryDragEvent event) {
		try {
			if (event.getNewItems().containsKey(22)) {
				event.setCancelled(true);
				event.getView().getTopInventory().setItem(22, event.getNewItems().get(22));
				new BukkitRunnable(){
					@Override
					public void run () {
						if(event.getType() == DragType.SINGLE){
							ItemStack cursor = event.getWhoClicked().getOpenInventory().getCursor();
							cursor.setAmount(cursor.getAmount()-1);
							event.getWhoClicked().getInventory().addItem(cursor);
						}
						event.getWhoClicked().getOpenInventory().setCursor(null);
						cancel();
					}
				}.runTaskTimer(Main.getInstance(), 1, 0);
				inventoryInteractEvent(new InventoryClickEvent(event.getView(), InventoryType.SlotType.CONTAINER, 22, ClickType.LEFT, InventoryAction.PLACE_ALL));
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

	/* Events for removing the player from the openInventories list if they exit the inventory */
	@EventHandler
	public void onClose (InventoryCloseEvent event) {
		try {
			Player player = (Player) event.getPlayer();
			UUID uuid = player.getUniqueId();
			if (InventoryGUI.getOpenInventories().containsKey(uuid)) {
				InventoryGUI.getInventoriesByUUID().get(InventoryGUI.getOpenInventories().get(uuid)).closeExecutions(player);
				InventoryGUI.getOpenInventories().remove(uuid);
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

	@EventHandler
	public void onQuit (PlayerQuitEvent event) {
		try {
			Player player = event.getPlayer();
			UUID uuid = player.getUniqueId();
			if (InventoryGUI.getOpenInventories().containsKey(uuid)) {
				InventoryGUI.getInventoriesByUUID().get(InventoryGUI.getOpenInventories().get(uuid)).closeExecutions(player);
				InventoryGUI.getOpenInventories().remove(uuid);
			}
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
	}

}
