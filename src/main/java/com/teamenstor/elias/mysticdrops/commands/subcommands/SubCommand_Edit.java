package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.managers.GUIManager;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SubCommand_Edit extends SubCommand {


	public SubCommand_Edit (CommandBase base) {
		super(base, "edit", "mysticdrops.drops.edit", "Configure what will drop what.");
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) {
				sender.sendMessage(MessageUtility.noPermission());
			} else if (!(sender instanceof Player)) {
				sender.sendMessage(ChatColor.RED + "Only players can execute this command.");
			} else if (args.length > 0) { sendCommandHelp(sender); } else {
				Player player = (Player) sender;
				if (player.getGameMode() != GameMode.CREATIVE) {
					player.sendMessage(ChatColor.RED + "You must be in creative mode to access this command.");
				} else GUIManager.getMainMenu().open(player);
			}
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
