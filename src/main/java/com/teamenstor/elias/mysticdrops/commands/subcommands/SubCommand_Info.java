package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SubCommand_Info extends SubCommand {

	public SubCommand_Info (CommandBase base) {
		super(base, "info", "mysticdrops.info", "Display plugin information.");
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) {
				sender.sendMessage(MessageUtility.noPermission());
				return true;
			}
			if (args.length > 0) {
				sendCommandHelp(sender);
				return true;
			}
			MessageUtility.sendPluginInfo(sender);
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
