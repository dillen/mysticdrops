package com.teamenstor.elias.mysticdrops.commands.subcommands;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.utility.MessageUtility;
import com.teamenstor.elias.mysticdrops.utility.interfaces.CommandBase;
import com.teamenstor.elias.mysticdrops.utility.interfaces.SubCommand;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class SubCommand_Reload extends SubCommand {

	public SubCommand_Reload (CommandBase base) {
		super(base, "reload", "mysticdrops.reload", "Reloads the config.yml file.");
	}

	@Override
	public boolean execute (CommandSender sender, Command command, String label, String[] args) {
		try {
			if (!sender.hasPermission(getPermission())) {
				sender.sendMessage(MessageUtility.noPermission());
				return true;
			}
			if (args.length > 0) {
				sendCommandHelp(sender);
				return true;
			}
			sender.sendMessage(ChatColor.GREEN + "Reloading...");
			Main.configManager.getConfig("config.yml").reload();
			sender.sendMessage(ChatColor.GREEN + "Reloaded the config.yml file.");
			return true;
		} catch (Exception exception) { MessageUtility.logException(exception, getClass()); }
		return false;
	}
}
