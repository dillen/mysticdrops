package com.teamenstor.elias.mysticdrops.utility;

import com.teamenstor.elias.mysticdrops.Main;
import com.teamenstor.elias.mysticdrops.utility.interfaces.DropBase;
import org.bukkit.configuration.MemorySection;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class SaveUtility {

	public static boolean saveBlockDrops () {
		try {
			Main.configManager.getConfig("drops.yml").clear();

			System.out.println("BlockDrops: " + Main.blockDrops);

			if (Main.blockDrops.size() <= 0) {
				Main.configManager.getConfig("drops.yml").set("blocks.0.key", "");
				Main.configManager.getConfig("drops.yml").set("blocks.0.value", "");
			} else {
				AtomicInteger i = new AtomicInteger(0);
				Main.blockDrops.forEach((key, value) -> {
					Main.configManager.getConfig("drops.yml").set("blocks." + i.get() + ".key", key);
					Main.configManager.getConfig("drops.yml").set("blocks." + i.get() + ".value", value);
					i.getAndIncrement();
				});
			}

			Main.configManager.getConfig("drops.yml").save();
			return true;
		} catch (Exception exception) {
			MessageUtility.logException(exception, SaveUtility.class);
			return false;
		}
	}

	public static boolean loadBlockDrops () {
		try {
			LinkedHashMap<ItemStack, DropBase> output = new LinkedHashMap<>();
			MemorySection memorySection = (MemorySection) Main.configManager.getConfig("drops.yml").get("blocks");

			List<String> inputs = new ArrayList<>(memorySection.getValues(false).keySet());
			inputs.forEach(input -> {
				try {
					output.put((ItemStack) Main.configManager.getConfig("drops.yml").get("blocks." + input + ".key"), (DropBase) Main.configManager.getConfig("drops.yml").get("blocks." + input + ".value"));
				} catch (Exception ignored) {}
			});
			output.remove(null);
			Main.blockDrops = output;
			return true;
		} catch (Exception exception) {
			MessageUtility.logException(exception, SaveUtility.class);
			return false;
		}
	}

}
